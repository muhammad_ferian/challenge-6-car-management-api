swagger: "2.0"
info:
  title: "API Documentation of Car Management"
  description: ""
  version: "1.0.0"
host: "localhost:3000"
basePath: "/api"
tags:
  - name: "user"
    description: ""
  - name: "car"
    description: ""
paths:
  /login:
    post:
      tags:
      - "user"
      summary: "login"
      description: ""
      produces:
        - "application/json"
      parameters:
        - name: "body"
          in: "body"
          required: true
          schema:
            type: "object"
            required:
              - "email"
              - "password"
            properties:
              email:
                type: "string"
                example: "admin@gmail.com"
              password:
                type: "string"
                example: "your password"
      responses:
        "201":
          description: "Successfull Operation"
          schema:
            items:
              type: "object"
              properties:
                status:
                  type: "string"
                  default: "success"
                data:
                  type: "object"
                  properties:
                    id:
                      type: "integer"
                    role:
                      type: "string"
                    email:
                      type: "string"
                    token:
                      type: "string"
        "401":
          description: "Invalid email or password"
        "404":
          description: "User not found"
        "500":
          description: "Server Error"

  /register/admin:
    post:
      tags: 
        - "user"
      security: 
        - bearerAuth: []
      summary: "create an admin, only for superadmin"
      description: ""
      produces:
        - "application/json"
      parameters:
        - name: "body"
          in: "body"
          required: true
          schema:
            type: "object"
            required:
              - "email"
              - "password"
            properties:
              email:
                type: "string"
                example: "admin@gmail.com"
              password:
                type: "string"
                example: "your password" 
      responses:
        "201":
          description: "Successfull Operation"
          schema:
            items:
              $ref: "#/definitions/UserResponses"
        "401":
          description: "Unauthorized"
        "422":
          description: "Unprocessable Entity"
        "500":
          description: "Server Error"

  /register/member:
    post:
      tags:
        - "user"
      summary: "create a member"
      description: ""
      produces:
        - "application/json"
      parameters:
        - name: "body"
          in: "body"
          required: true
          schema:
            type: "object"
            required:
              - "email"
              - "password"
            properties:
              email:
                type: "string"
                example: "member@gmail.com"
              password:
                type: "string"
                example: "your password" 
      responses:
        "201":
          description: "Successfull Operation"
          schema:
            items:
              $ref: "#/definitions/UserResponses"
        "422":
          description: Unprocessable Entity
        "500":
          description: "Server Error"
  
  /user/{id}:
    get:
      tags:
        - "user"
      security: 
        - bearerAuth: []
      summary: "show a data user by id"
      description: ""
      produces:
        - "application/json"
      parameters:
        - name: "id"
          in: "path"
          required: true
          schema:
            type: "integer"
      responses:
        "201":
          description: "Successfull Operation"
          schema:
            items:
              $ref: "#/definitions/UserResponses"
        "401":
          description: "Unauthorized"
        "422":
          description: "Unprocessable Entity"
        "500":
          description: "Server Error"
    
    put:
      tags:
        - "user"
      security: 
        - bearerAuth: []
      summary: "update data user"
      description: ""
      produces:
        - "application/json"
      parameters:
        - name: "id"
          in: "path"
          required: true
          schema:
            type: "integer"
        - name: "body"
          in: "body"
          required: true
          schema:
            type: "object"
            required:
              - "email"
              - "password"
            properties:
              email:
                type: "string"
                example: "member@gmail.com"
              password:
                type: "string"
                example: "your password" 
      responses:
        "201":
          description: "Successfull Operation"
          schema:
            items:
              $ref: "#/definitions/UpdateResponses"
        "401":
          description: "Unauthorized"
        "422":
          description: "Unprocessable Entity" 
        "500":
          description: "Server Error"

    delete:
      tags:
        - "user"
      security: 
        - bearerAuth: []
      summary: "delete data user"
      description: ""
      produces:
        - "application/json"
      parameters:
        - name: "id"
          in: "path"
          required: true
          schema:
            type: "integer"
      responses:
        "200":
          description: "Successfull Operation"
          schema:
            items:
              $ref: "#/definitions/DeleteResponses"
        "401":
          description: "Unauthorized"
        "422":
          description: "Unprocessable Entity"
        "500":
          description: "Server Error"

  /user:
    get:
      tags:
        - "user"
      security: 
        - bearerAuth: []
      summary: "show user who are logged in"
      description: ""
      produces:
        - "application/json"
      responses:
        "200":
          description: "Successfull Operation"
          schema:
            items:
              $ref: "#/definitions/UserResponses"
        "401":
          description: "Unauthorized"
        "422":
          description: "Unprocessable Entity"
        "500":
          description: "Server Error"

  /users:
    get:
      tags:
        - "user"
      security: 
        - bearerAuth: []
      summary: "show all users"
      description: ""
      produces:
        - "application/json"
      responses:
        "200":
          description: "Successfull Operation"
          schema:
            items:
              $ref: "#/definitions/UserResponses"
        "401":
          description: "Unauthorized"
        "422":
          description: "Unprocessable Entity"
        "500":
          description: "Server Error"

  /cars:
    get:
      tags:
        - "car"
      security: 
        - bearerAuth: []
      summary: "show all cars"
      description: ""
      produces:
        - "application/json"
      responses:
        "200":
          description: "Successfull Operation"
          schema:
            items:
              $ref: "#/definitions/CarResponses"
        "401":
          description: "Unauthorized"
        "422":
          description: "Unprocessable Entity"
        "500":
          description: "Server Error"

  /cars/available:
    get:
      tags:
        - "car"
      security: 
        - bearerAuth: []
      summary: "show all cars are available"
      description: ""
      produces:
        - "application/json"
      responses:
        "200":
          description: "Successfull Operation"
          schema:
            items:
              $ref: "#/definitions/CarResponses"
        "401":
          description: "Unauthorized"
        "422":
          description: "Unprocessable Entity"
        "500":
          description: "Server Error"
  
  /car:
    post:
      tags:
        - "car"
      security: 
        - bearerAuth: []
      summary: "create a car"
      description: ""
      produces:
        - "application/json"
      parameters:
        - name: "body"
          in: "body"
          required: true
          schema:
            type: "object"
            required:
              - "name"
              - "price"
              - "size"
              - "available"
            properties:
              name:
                type: "string"
                example: "bmw"
              price:
                type: "float"
                example: 500000
              size:
                type: "string"
                enum:
                  - "small"
                  - "medium"
                  - "large"
              available:
                type: "boolean"
                example: true
      responses:
        "201":
          description: "Successfull Operation"
          schema:
            type: "array"
            items:
              type: "object"
              properties:
                status: 
                  type: "string"
                  default: "success"
                data:
                  type: "object"
                  properties:
                    id:
                      type: "integer"
                    name:
                      type: "string"
                    price:
                      type: "number"
                    size:
                      type: "string"
                    available:
                      type: "boolean"
                    createdBy:
                      type: "string"
                    createdAt:
                      type: "string"
                      example: "2022-05-12T13:07:16.901Z"
                    updatedAt:
                      type: "string"
                      example: "2022-05-12T13:07:16.901Z"
                    updatedBy:
                      type: "string"
                    deletedBy:
                      type: "string"
        "401":
          description: "Unauthorized"
        "422":
          description: "Unprocessable Entity"
        "500":
          description: "Server Error"

  /car/{id}:
    put:
      tags:
        - "car"
      security: 
        - bearerAuth: []
      summary: "update data car"
      description: ""
      produces:
        - "application/json"
      parameters:
        - name: "id"
          in: "path"
          required: true
          schema:
            type: "integer"
        - name: "body"
          in: "body"
          required: true
          schema:
            type: "object"
            required:
              - "name"
              - "price"
              - "size"
              - "available"
            properties:
              name:
                type: "string"
                example: "bmw"
              price:
                type: "float"
                example: 500000
              size:
                type: "string"
                enum:
                  - "small"
                  - "medium"
                  - "large"
              available:
                type: "boolean"
                example: true
      responses:
        "201":
          description: "Successfull Operation"
          schema:
            items:
              $ref: "#/definitions/UpdateResponses"
        "401":
          description: "Unauthorized"
        "422":
          description: "Unprocessable Entity"
        "500":
          description: "Server Error"

    delete:
      tags:
        - "car"
      security: 
        - bearerAuth: []
      summary: "delete data car"
      description: ""
      produces:
        - "application/json"
      parameters:
        - name: "id"
          in: "path"
          required: true
          schema:
            type: "integer"
      responses:
        "200":
          description: "Successfull Operation"
          schema:
            items:
              $ref: "#/definitions/DeleteResponses"
        "401":
          description: "Unauthorized"
        "422":
          description: "Unprocessable Entity"
        "500":
          description: "Server Error"

securityDefinitions:
  bearerAuth:     
    name: Authorization
    type: apiKey
    scheme: bearer
    in: header

definitions:
  UserResponses:
    type: "object"
    properties:
      status:
        type: "string"
        default: "success"
      data:
        type: "object"
        properties:
          id:
            type: "integer"
          role:
            type: "string"
          email:
            type: "string"
          password:
            type: "string"
          createdAt:
            type: "string"
            example: "2022-05-12T13:07:16.901Z"
          updatedAt:
            type: "string"
            example: "2022-05-12T13:07:16.901Z"

  UpdateResponses:
    type: "object"
    properties:
      status:
        type: "string"
        default: "success"
      message:
        type: "string"
        default: "User updated successfully"  

  DeleteResponses:
    type: "object"
    properties:
      status:
        type: "string"
        default: "success"
      message:
        type: "string"
        default: "User deleted successfully"

  CarResponses:
    type: "object"
    properties:
      status:
        type: "string"
        default: "success"
      data:
        type: "object"
        properties:
          id:
            type: "integer"
          name:
            type: "string"
          price:
            type: "number"
          size:
            type: "string"
          available:
            type: "boolean"
          createdBy:
            type: "string"
          updatedBy:
            type: "string"
          deletedBy:
            type: "string"
          createdAt:
            type: "string"
            example: "2022-05-12T13:07:16.901Z"
          updatedAt:
            type: "string"
            example: "2022-05-12T13:07:16.901Z"