const { car } = require("../models")

const create = (value) => car.create(value)
const findAll = () => car.findAll()
const find = (id) => car.find(id)
const findAvailableCars = () => car.findAll({where: {available: true}})
const update = (id, value) => car.update(value, { where: { id } })
const deleteCars = (id) => car.destroy({ where: { id } })

module.exports = { create, findAll, find, findAvailableCars, update, deleteCars }
