const { user } = require("../models")

const create = (value) => user.create(value)
const findAll = () => user.findAll()
const find = (id) => user.findByPk(id)
const findOne = (email) => user.findOne({ where: { email } })
const update = (id, value) => user.update(value, { where: { id } })
const deleteUser = (id) => user.destroy({ where: { id } })

module.exports = { create, findAll, find, findOne, update, deleteUser }