const express = require("express")
const path = require("path")
const controllers = require("../app/controllers")
const authController = controllers.api.authController
const usersController = controllers.api.usersController
const carsController = controllers.api.carsController
const router = express.Router()

router.post("/register/member", usersController.register)
router.post("/register/admin",authController.authorize, usersController.registerAdmin)
router.post("/login", authController.login)
router.get("/user/:id", authController.authorize, usersController.getUser)
router.put("/user/:id", authController.authorize, usersController.update)
router.delete("/user/:id", authController.authorize, usersController.deleteUser)
router.get("/user", authController.authorize, usersController.getCurrentUser)
router.get("/users", authController.authorize, usersController.getUsers)

router.get("/cars/available", authController.authorize, carsController.getAvailableCars)
router.get("/cars", authController.authorize,carsController.getCars)
router.post("/car", authController.authorize,carsController.createCar)
router.put("/car/:id", authController.authorize, carsController.updateCar)
router.delete("/car/:id", authController.authorize, carsController.deleteCar)

module.exports = router