'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "users",
      [
        {
          role: "superadmin",
          email: "admin@ymail.com",
          password: "$2b$10$zZE7NV3RCm.O0ypvzUMcv.kOlLxGSVnoggc8ay5ui/5rSbTvgeXSK", // 123
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    )
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete("users", null, {})
  }
}