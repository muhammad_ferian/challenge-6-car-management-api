## Challenge 6 : Car Management API

### Database

- `npx sequelize-cli db:create`: create database
- `npx sequelize-cli db:migrate`: run database migrate
- `npx sequelize-cli db:seed:all`: run seed (add superadmin account)
- `npx sequelize-cli db:drop`: delete database
- `npx sequelize-cli db:migrate:undo`: undo last migration

### Run Server
```
yarn dev
```

### API Documentations : Swagger UI
```
http://localhost:3000/api-docs/
```

### Superadmin Account
Run `npx sequelize-cli db:seed:all` first before login with superadmin account.
```
email: admin@ymail.com
password: 123
```
